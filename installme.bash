#!/bin/bash
install_dir=miniconda-install
setup/Miniconda3-latest-Linux-x86_64.sh -b -p $install_dir
export PATH=${PATH}:${install_dir}/bin
${install_dir}/bin/conda env create -f setup/environment.yml
source ${install_dir}/bin/activate venv
